import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const createUser = () => {
  return prisma.user.create({
    data: {
      name: "Alice",
      email: "alice@prisma.io",
      posts: {
        create: { title: "Hello World" },
      },
      profile: {
        create: { bio: "I like turtles" },
      },
    },
  });
};

const updatePost = () => {
  return prisma.post.update({
    where: { id: 1 },
    data: { published: true },
  });
};

async function main() {
  // await createUser();

  // const users = await prisma.user.findMany({
  //   include: {
  //     posts: true,
  //     profile: true,
  //   },
  // });
  // console.log(users);
  const post = await updatePost();
  console.log(post);
}

main()
  .catch((e) => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
